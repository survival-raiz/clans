package com.gitlab.survivalRaiz.clans.config;

import api.skwead.storage.file.yml.YMLConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Set;

/**
 * Handles rank permissions.
 * When custom ranks get implemented these will be the default ranks' permissions
 */
public class ClanConfig extends YMLConfig {
    public ClanConfig(JavaPlugin plugin) {
        super("permissions", plugin);
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {

    }
}
