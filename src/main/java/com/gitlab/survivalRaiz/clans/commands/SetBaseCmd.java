package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.clans.clans.Rank;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SetBaseCmd extends ConfigCommand {

    private final Clans plugin;

    public SetBaseCmd(Clans plugin) {
        super("base", "define ou vai para a base do clan",
                "/base [definir]", new ArrayList<>(), "base");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (plugin.getClanManager().getPlayerClan(p.getUniqueId()) == null)
            throw new SRCommandException(p, Message.PLAYER_NEEDS_CLAN, plugin.getCore());

        final Rank playerRank = Rank.valueOf(plugin.getCore().getDbManager().hasClan(p.getUniqueId())[1]);

        if (strings.length > 0) {
            if (!strings[0].equals("definir"))
                throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

            if (playerRank.ordinal() < Rank.MODERATOR.ordinal())
                throw new SRCommandException(p, Message.NO_PERMISSIONS, plugin.getCore());

            return 1; // definir
        }

        return 0; // tp
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);

        final Player p = (Player) commandSender;

        if (syntax == 1) {
            plugin.getClanManager().updateClanBase(plugin.getClanManager().getPlayerClan(p.getUniqueId()),
                    p.getLocation().getBlockX(), p.getLocation().getBlockY(), p.getLocation().getBlockZ(),
                    p.getWorld().getName());
        } else {
            final Location base = plugin.getCore().getDbManager().getClanBase(
                    plugin.getClanManager().getPlayerClan(p.getUniqueId()));

            if (base == null)
                plugin.getCore().getMsgHandler().message(p, Message.CLAN_HAS_NO_BASE);
            else
                p.teleport(base);
        }
    }
}
