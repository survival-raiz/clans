package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class InviteCmd extends ConfigCommand {

    private final Clans plugin;

    public InviteCmd(Clans clans) {
        super("convidar", "convida um jogador para o clan",
                "/convidar <jogador>", new ArrayList<>(), "convidar");
        this.plugin = clans;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (strings.length < 1)
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        if (Bukkit.getPlayer(strings[0]) == null)
            throw new SRCommandException(p, Message.PLAYER_NOT_FOUND, plugin.getCore());

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        final UUID recruiter = ((Player) commandSender).getUniqueId();
        final UUID player = Bukkit.getPlayer(strings[0]).getUniqueId();

        plugin.getRecruitManager().sendInvitation(recruiter, player);
    }
}
