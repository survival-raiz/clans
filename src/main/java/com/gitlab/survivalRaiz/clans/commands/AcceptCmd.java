package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class AcceptCmd extends ConfigCommand {

    private final Clans plugin;

    public AcceptCmd(Clans plugin) {
        super("aceitar", "aceita o convite dum clan",
                "/aceitar <clan>", new ArrayList<>(), "aceitar");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (strings.length < 1)
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        try {
            Integer.parseInt(strings[0]);
        } catch (NumberFormatException e) {
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());
        }

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        final Player p = (Player) commandSender;
        String clan;

        try {
            clan = plugin.getRecruitManager().answer(p.getUniqueId(), Integer.parseInt(strings[0]));
        } catch (IndexOutOfBoundsException e) {
            throw new SRCommandException(p, Message.INVITE_NOT_FOUND, plugin.getCore());
        }

        plugin.getClanManager().accept(p.getUniqueId(), clan);
    }
}