package com.gitlab.survivalRaiz.clans.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class DelClanCmd extends ConfigCommand {

    private final Clans plugin;

    public DelClanCmd(Clans plugin) {
        super("delclan", "apaga o clan",
                "/delclan [clan]", new ArrayList<>(), "delclan");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player) && strings.length > 0)
            return 0;

        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.NO_PERMISSIONS, plugin.getCore());

        final Player p = (Player) commandSender;

        if (Perms.DEL_ANY_CLAN.allows(p) && strings.length > 0)
            return 0;

        if (Perms.DEL_ANY_CLAN.allows(p) && strings.length == 0)
            throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());

        if (plugin.getClanManager().isOwner(p.getUniqueId()))
            return 1;

        throw new SRCommandException(p, Message.NO_PERMISSIONS, plugin.getCore());

        // 0 -> delclan <name>
        // 1 -> delclan
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);

        plugin.getClanManager().deleteClan(syntax == 0 ?
                strings[0] :
                plugin.getClanManager().getPlayerClan(((Player) commandSender).getUniqueId()));
    }
}
