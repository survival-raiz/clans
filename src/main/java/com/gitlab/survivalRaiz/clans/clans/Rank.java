package com.gitlab.survivalRaiz.clans.clans;

/**
 * Lists all possible ranks a player can have in a clan.
 * In the future all clans will be able to customize these ranks and of course each rank's permissions
 */
public enum Rank {
    RECRUIT,
    MEMBER,
    MODERATOR,
    OWNER;

    public Rank getNext() {
        final int index = (this.ordinal() + 1) % Rank.values().length;

        return Rank.values()[index];
    }
}
